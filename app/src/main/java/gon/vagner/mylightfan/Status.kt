package gon.vagner.mylightfan

object Status {
    var lightOn : Boolean = false
    var fanOn : Boolean = false
    var lightTimerOn : Long = 0
    var lightTimerOff : Long = 0
    var fanTimerOn : Long = 0
    var fanTimerOff : Long = 0

    fun switchLight() : Boolean {
        fanOn = !fanOn
        return fanOn
    }

    fun switchFan() : Boolean {
        lightOn = !lightOn
        return lightOn
    }
}
