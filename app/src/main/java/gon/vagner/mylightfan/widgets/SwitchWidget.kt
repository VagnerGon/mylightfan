package gon.vagner.mylightfan.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.Toast
import gon.vagner.mylightfan.Connectivity
import gon.vagner.mylightfan.R
import gon.vagner.mylightfan.Status

private const val LIGHT_ACTION : String = "LIGHT_ACTION"
private const val FAN_ACTION = "FAN_ACTION"

class SwitchWidget : AppWidgetProvider() {

    private val status : Status = Status

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        val remoteView = RemoteViews(context.packageName, R.layout.switchers_widget)

        Connectivity(context).readEsp(status)

        createButtonIntent(context, remoteView, R.id.sw_light_widget, LIGHT_ACTION)
        createButtonIntent(context, remoteView, R.id.sw_fan_widget, FAN_ACTION)

        appWidgetManager.updateAppWidget(appWidgetIds, remoteView)
    }

    private fun createButtonIntent(context: Context, remoteView: RemoteViews, rButton: Int, action: String) {
        val intent = Intent(context, SwitchWidget::class.java)
        intent.action = action
        remoteView.setOnClickPendingIntent(rButton, PendingIntent.getBroadcast(context, 0, intent, 0))
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        try {
            if (intent.action == LIGHT_ACTION) {
                status.lightOn = !status.lightOn
                Connectivity(context).turnLightOnOff(status.lightOn)
            }
            if (intent.action == FAN_ACTION) {
                status.fanOn = !status.fanOn
                Connectivity(context).turnFanOff(status.fanOn)
            }
        } catch (e : Exception) {
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }
}
