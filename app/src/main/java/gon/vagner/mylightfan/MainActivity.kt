package gon.vagner.mylightfan

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import java.util.*
import kotlin.concurrent.timer

class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var timer: Timer

    private var ipInfo : TextView? = null
    private lateinit var lightStatus : TextView
    private lateinit var fanStatus: TextView

    private val status : Status = Status

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (!key.equals(Preferences.IP_ADDRESS))
            return
        ipInfo?.text = sharedPreferences?.getString(key, "not set")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.button3)
        btn.setOnClickListener{
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        ipInfo = findViewById(R.id.ip_info)
        ipInfo?.text = Preferences(this).ipAddress
        lightStatus = findViewById(R.id.lightStatus)
        fanStatus = findViewById(R.id.fanStatus)
    }

    private fun backgroundCheckTask() {
        Connectivity(this@MainActivity).readEsp(status)
        updateUiFromStats(status)
    }


    private fun updateUiFromStats(status: Status) {
        // electic installations is wrong
        this@MainActivity.runOnUiThread {
            lightStatus.text = if (status.fanOn) getString(R.string.on) else getString(R.string.off)
            fanStatus.text = if (status.lightOn) getString(R.string.on) else getString(R.string.off)
        }
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }

    override fun onResume() {
        super.onResume()
        ipInfo?.text = Preferences(this).ipAddress
        val task = object: TimerTask() {
            override fun run() {
                backgroundCheckTask()
            }
        }
        timer = Timer()
        timer.schedule(task, 500, 3500)
    }

    fun setLightOnOff(view: View) {
        Connectivity(this).turnLightOnOff(status.switchLight())
        updateUiFromStats(status)
    }
    fun setFanOnOff(view: View) {
        Connectivity(this).turnFanOff(status.switchFan())
        updateUiFromStats(status)
    }
    fun setFanTimer(view: View) {
        val cal = Calendar.getInstance()
//        cal.set(Calendar.HOUR_OF_DAY, timePicker?.hour!!)
//        cal.set(Calendar.MINUTE, timePicker?.minute!!)
        val timeDiff = cal.timeInMillis - Calendar.getInstance().timeInMillis
        if (timeDiff < 0)
            Toast.makeText(this, "Invalid date", Toast.LENGTH_SHORT).show()

        status.fanTimerOn = timeDiff
        Connectivity(this).setFanTimer(timeDiff, true)
    }
}

