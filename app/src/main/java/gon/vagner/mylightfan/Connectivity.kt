package gon.vagner.mylightfan

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

class Connectivity(private val context: Context) {

    private val queue : RequestQueue = Volley.newRequestQueue(context)
    private val url : String = Preferences(context).ipAddress

    fun readEsp (status : Status) {
        sendToESP("", status)
    }

    fun sendToESP(message : String, status : Status? = null) {
        var request = "http://$url"
        if (message != "")
            request += "?$message"
        queue.add(JsonObjectRequest(request,
            null,
            Response.Listener { response ->
                if (status != null) {
                    status.lightOn = response.getInt("lightOn") == 0
                    status.fanOn = response.getInt("fanOn") == 0
                    status.lightTimerOn = response.getJSONObject("lightTimer").getLong("on")
                    status.lightTimerOff = response.getJSONObject("lightTimer").getLong("off")
                    status.fanTimerOn = response.getJSONObject("fanTimer").getLong("on")
                    status.fanTimerOff = response.getJSONObject("fanTimer").getLong("off")
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(context, "Error! $error", Toast.LENGTH_SHORT).show()
            }))
    }

    fun turnLightOnOff(onOff: Boolean) {
        @Suppress("SpellCheckingInspection")
        sendToESP("Rele2_${if(onOff) "on" else "off"}")
    }

    fun turnFanOff(onOff: Boolean) {
        @Suppress("SpellCheckingInspection")
        sendToESP("Rele1_${if(onOff) "on" else "off"}")
    }

    fun setFanTimer(date : Long, onOff : Boolean) {
        sendToESP("Timer1${if(onOff) "ON" else "OFF"}=$date$")
    }

    fun setLightTimer(date: Long, onOff: Boolean) {
        sendToESP("Timer2${if(onOff) "ON" else "OFF"}=$date$")
    }
}