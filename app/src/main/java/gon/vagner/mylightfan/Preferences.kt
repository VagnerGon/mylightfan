package gon.vagner.mylightfan

import android.content.Context
import android.content.SharedPreferences

class Preferences (context: Context) {
    private val prefsFilename = this.javaClass.`package`?.name

    private val prefs: SharedPreferences = context.getSharedPreferences(prefsFilename, 0)

    var ipAddress: String
        get() = prefs.getString(IP_ADDRESS, "192.168.0.1") ?: ""
        set(value) = prefs.edit().putString(IP_ADDRESS, value).apply()

    fun getPreferences() : SharedPreferences {
        return prefs
    }

    companion object {
        const val IP_ADDRESS = "ip_address"
    }
}